
package main.java.KhmerGeoQuiz;

import java.lang.ClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;
import java.lang.Exception;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import main.java.KhmerGeoQuiz.QuizQuestionParser;
import main.java.KhmerGeoQuiz.ReturnResult;
import main.java.KhmerGeoQuiz.Model.QuizProgress;
import main.java.KhmerGeoQuiz.Model.QuizQuestion;
import main.java.KhmerGeoQuiz.Model.UserQuizAnswer;

final public class AppState {
  QuizQuestion[] quizQuestions;

  HashMap<Integer, UserQuizAnswer> userQuizAnswers;

  boolean areAllQuestionAnswered;

  QuizProgress quizProgress;

  PropertyChangeSupport watcher;

  public AppState() {
    this.areAllQuestionAnswered = false;

    try {
      ReturnResult parseXMLResult =
        QuizQuestionParser.parseXML(
          ClassLoader.getSystemClassLoader().getResource(
            "questions.xml").toURI());

      QuizQuestionParser.Quiz quiz =
        (QuizQuestionParser.Quiz) parseXMLResult.payload;

      List<QuizQuestion> quizQuestionList = quiz.getQuizQuestion();

      this.quizQuestions =
        Arrays.copyOf(
          quizQuestionList.toArray(),
          quizQuestionList.size(),
          QuizQuestion[].class
        );

      this.initDefaultState(this.quizQuestions.length);

      this.watcher = new PropertyChangeSupport(this.quizProgress);
    } catch (Exception exception) {
      exception.printStackTrace();

      System.exit(1);
    }
  }

  void initDefaultState(final int quizQuestionLength) {
    this.quizProgress = new QuizProgress(quizQuestionLength);
    this.userQuizAnswers = new HashMap<Integer, UserQuizAnswer>();
  }

  public void listenTo(PropertyChangeListener listener) {
    this.watcher.addPropertyChangeListener(listener);
  }
  
  public void unlistenTo(PropertyChangeListener listener) {
    this.watcher.removePropertyChangeListener(listener);
  }

  public void stateChange(
    final String stateValue,
    final Object oldValue,
    final Object newValue
  ) {
    this.watcher.firePropertyChange(stateValue, oldValue, newValue);
  }

  final public QuizQuestion[] getQuizQuestions() { return this.quizQuestions; }

  public void quizDone() { this.areAllQuestionAnswered = true; }

  public boolean isAllQuestionDone() { return this.areAllQuestionAnswered; }

  public QuizProgress getQuizProgress() { return this.quizProgress; }

  public int numberOfQuestion() { return this.quizQuestions.length; }

  public HashMap<Integer, UserQuizAnswer> getUserAnswers() {
    return this.userQuizAnswers;
  }

  public QuizQuestion getQuizQuestion(final int id) {
    if (this.quizQuestions.length <= 0
      || this.quizQuestions.length <= id
      || id < 0) {
      return null;
    }

    return this.quizQuestions[id];
  }

  public int getCurrentQuizId() {
    if (this.quizProgress == null) {
      return -1;
    }

    return this.quizProgress.currentQuestionIndex;
  }
}
