
package main.java.KhmerGeoQuiz.Model;

import main.java.KhmerGeoQuiz.Model.UserQuizAnswer;
import main.java.KhmerGeoQuiz.Model.QuizProgress;

public class QuizQuestion {
  int id;
  String statement;
  String questionImageUrl;
  boolean isStatementTrue;

  public QuizQuestion() {}

  public QuizQuestion(
    final int id,
    final String statement,
    final String questionImageUrl,
    final boolean isStatementTrue
  ) {
    this.statement = statement;
    this.questionImageUrl = questionImageUrl;
    this.id = id;
    this.isStatementTrue = isStatementTrue;
  }

  final public boolean isUserAnswerCorrect(
    UserQuizAnswer[] userQuizAnswer,
    QuizProgress quizProgress
  ) {
    return
      quizProgress.currentQuestionIndex <= this.id
        && userQuizAnswer[this.id].isStatementAgreed() == this.isStatementTrue;
  }

  final public boolean isCorrectAnswer(UserQuizAnswer userQuizAnswer) {
    return userQuizAnswer.isStatementAgreed() == this.isStatementTrue;
  }

  final public int quizQuestionId() { return this.id; }

  final public String quizQuestionStatement() { return this.statement; }

  final public String quizQuestionImageUrl() { return this.questionImageUrl; }
}
