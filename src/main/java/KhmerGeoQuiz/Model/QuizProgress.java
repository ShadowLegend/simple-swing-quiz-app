
package main.java.KhmerGeoQuiz.Model;

import main.java.KhmerGeoQuiz.ReturnResult;

public class QuizProgress {
  final int totalQuestionNumber;
  public double progressAsPercent;
  public int numberOfQuestionAnswered;
  public int currentQuestionIndex;

  public QuizProgress(final int totalQuestionNumber) {
    this.totalQuestionNumber = totalQuestionNumber;
    this.progressAsPercent = .0;
    this.currentQuestionIndex = 0;
    this.numberOfQuestionAnswered = 0;
  }

  void navigateQuestion(final int offset) {
    final int newIndex = this.currentQuestionIndex + offset;

    if (newIndex < this.totalQuestionNumber && newIndex > -1) {
      this.currentQuestionIndex = newIndex;
      this.progressAsPercent =
        this.currentQuestionIndex / this.totalQuestionNumber;
    }
  }

  public void nextQuestion() { this.navigateQuestion(1); }

  public void previousQuestion() { this.navigateQuestion(-1); }

  final public void resetQuizProgressState() {
    this.currentQuestionIndex = 0;
    this.progressAsPercent = 0;
  }
}
