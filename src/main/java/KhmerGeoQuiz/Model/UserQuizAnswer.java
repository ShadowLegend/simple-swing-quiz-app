
package main.java.KhmerGeoQuiz.Model;

final public class UserQuizAnswer {
  final int quizQuestionId;
  final boolean isUserAgreeToStatement;

  public UserQuizAnswer(
    final int quizQuestionId,
    final boolean isUserAgreeToStatement
  ) {
    this.quizQuestionId = quizQuestionId;
    this.isUserAgreeToStatement = isUserAgreeToStatement;
  }

  final public int quizQuestionId() { return this.quizQuestionId; }

  final public boolean isStatementAgreed() { return this.isUserAgreeToStatement; }
}
