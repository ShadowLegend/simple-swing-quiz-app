
package main.java.KhmerGeoQuiz;

import java.lang.Exception;

public class ReturnResult {
  final public Object payload;
  final public Exception error;
  final public boolean isOk;

  ReturnResult() {
    this.isOk = true;
    this.error = null;
    this.payload = null;
  }

  ReturnResult(final Object payload) {
    this.isOk = true;
    this.error = null;
    this.payload = payload;
  }

  ReturnResult(final Exception exception) {
    this.isOk = false;
    this.error = exception;
    this.payload = null;
  }
}
