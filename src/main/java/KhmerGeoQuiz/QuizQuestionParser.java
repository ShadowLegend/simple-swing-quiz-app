package main.java.KhmerGeoQuiz;

import java.io.File;
import java.net.URI;
import java.io.FileNotFoundException;
import java.util.List;

import org.simpleframework.xml.Root;
import org.simpleframework.xml.Default;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.core.Persister;

import main.java.KhmerGeoQuiz.Model.QuizQuestion;
import main.java.KhmerGeoQuiz.ReturnResult;

public class QuizQuestionParser {
  final static String QUESTION = "Question";
  final static String ID = "Id";
  final static String STATEMENT = "Statement";
  final static String IMAGE_URL = "ImageUrl";
  final static String IS_STATEMENT_AGREE = "IsAgree";

  @Default
  @Root(name="quiz")
  final public static class Quiz {
    @ElementList(name="quizQuestion", inline=true, required=false)
    List<QuizQuestion> quizQuestions;

    public List<QuizQuestion> getQuizQuestion() { return this.quizQuestions; }
  }

  public static ReturnResult parseXML(final URI pathToXML) {
    try {
      Persister persister = new Persister();
      File xmlFile = new File(pathToXML);

      Quiz quiz = persister.read(Quiz.class, xmlFile);

      return new ReturnResult(quiz);
    } catch (FileNotFoundException exception) {
      return new ReturnResult(exception);
    } catch (Exception exception) {
      return new ReturnResult(exception);
    }
  }
}
