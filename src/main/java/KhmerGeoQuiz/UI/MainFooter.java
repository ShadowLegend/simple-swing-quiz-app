
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JPanel;

import main.java.KhmerGeoQuiz.AppState;
import main.java.KhmerGeoQuiz.UI.UIComponent;
import main.java.KhmerGeoQuiz.UI.Button;
import main.java.KhmerGeoQuiz.UI.TextBox;
import main.java.KhmerGeoQuiz.Model.QuizQuestion;
import main.java.KhmerGeoQuiz.Model.UserQuizAnswer;

public class MainFooter extends UIComponent<JPanel> {
  AppState appState;

  public MainFooter(AppState appState) {
    this.appState = appState;
    this.component = new JPanel();

    this.setup();
  }

  void setup() {
    this.setupQuizNavigateButton("Previous", false);
    this.setupQuizNavigateButton("Next", true);

    this.setupUserAnswerResultText();

    this.setupAnswerAggreementButton("True", true);
    this.setupAnswerAggreementButton("False", false);
  }

  void setupUserAnswerResultText() {
    TextBox textBox = new TextBox("Answer: ---");

    this.appState.listenTo(appStateChanges -> {
      final String stateChanges = appStateChanges.getPropertyName();

      switch(stateChanges) {
        case "QUIZ_PROGRESS:NAVIGATE_QUESTION": {
          QuizQuestion quizQuestion =
            (QuizQuestion) appStateChanges.getNewValue();

          if (!this.appState.getUserAnswers().containsKey(quizQuestion.quizQuestionId())) {
            textBox.update("Answer: ---");
          } else {
            textBox.update(
              this.appState.getUserAnswers().get(quizQuestion.quizQuestionId()).isStatementAgreed()
                ? "Answer: True"
                : "Answer: False"
            );
          }

          break;
        }

        case "QUIZ_ANSWER:ANSWER": {
          textBox.update(
            String.format("Answer: %s", String.valueOf(appStateChanges.getNewValue())));
          break;
        }

        default: {
          break;
        }
      }
    });

    this.component.add(textBox.internalComponent());
  }

  void setupAnswerAggreementButton(
    final String label,
    final boolean isAgree
  ) {
    final Button button = new Button(label);

    button.onClick(event -> {
      if (!this.appState.getUserAnswers().containsKey(this.appState.getCurrentQuizId())) {
        this.appState.getUserAnswers().put(
          this.appState.getCurrentQuizId(),
          new UserQuizAnswer(this.appState.getCurrentQuizId(), isAgree)
        );

        this.appState.stateChange("QUIZ_ANSWER:ANSWER", "", isAgree ? "True" : "False");

        if (this.appState.getUserAnswers().size() == this.appState.numberOfQuestion()) {
          this.appState.quizDone();

          this.appState.stateChange("QUIZ:FINISH", false, true);
        }
      }
    });

    this.component.add(button.internalComponent());
  }

  void setupQuizNavigateButton(
    final String label,
    final boolean isNavigateNext
  ) {
    final Button button = new Button(label);

    button.onClick(event -> {
      final int previousQuestionId = this.appState.getCurrentQuizId();

      if (isNavigateNext) {
        this.appState.getQuizProgress().nextQuestion();
      } else {
        this.appState.getQuizProgress().previousQuestion();
      }

      final int nextQuestionId = this.appState.getCurrentQuizId();

      this.appState.stateChange(
        "QUIZ_PROGRESS:NAVIGATE_QUESTION",
        this.appState.getQuizQuestion(previousQuestionId),
        this.appState.getQuizQuestion(nextQuestionId)
      );
    });

    this.component.add(button.internalComponent());
  }
}
