
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JPanel;
import java.awt.GridLayout;

import main.java.KhmerGeoQuiz.AppState;
import main.java.KhmerGeoQuiz.Model.QuizQuestion;
import main.java.KhmerGeoQuiz.UI.QuestionStatement;
import main.java.KhmerGeoQuiz.UI.ProgressBar;
import main.java.KhmerGeoQuiz.UI.UIComponent;

public class MainHeader extends UIComponent<JPanel> {
  AppState appState;
  GridLayout layout;

  public MainHeader(final AppState appState) {
    this.component = new JPanel();
    this.layout = new GridLayout(2, 1);
    this.appState = appState;

    this.component.setAlignmentX(JPanel.CENTER_ALIGNMENT);
    this.component.setAlignmentY(JPanel.CENTER_ALIGNMENT);
    this.component.setLayout(this.layout);

    this.setup();
  }

  void setup() {
    this.setupQuestionStatement();

    this.setupProgressBar();
  }

  void setupQuestionStatement() {
    QuestionStatement questionStatement =
      new QuestionStatement(
        this.appState.getQuizQuestion(
          this.appState.getCurrentQuizId()).quizQuestionStatement());

    this.appState.listenTo(appStateChanges -> {
      if (appStateChanges.getPropertyName().equals(
          "QUIZ_PROGRESS:NAVIGATE_QUESTION")) {
        questionStatement.update(
          ((QuizQuestion) appStateChanges.getNewValue()).quizQuestionStatement());
      }
    });

    this.component.add(questionStatement.internalComponent());
  }

  void setupProgressBar() {
    ProgressBar progressBar = new ProgressBar(this.appState);

    this.appState.listenTo(appStateChanges -> {
      if (appStateChanges.getPropertyName().equals("QUIZ_ANSWER:ANSWER")) {
        progressBar.update(this.appState.getUserAnswers().size());
      }
    });

    this.component.add(progressBar.internalComponent());
  }
}
