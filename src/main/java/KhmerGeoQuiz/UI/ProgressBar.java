
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JProgressBar;

import main.java.KhmerGeoQuiz.AppState;
import main.java.KhmerGeoQuiz.UI.UIComponent;

public class ProgressBar extends UIComponent<JProgressBar> {
  AppState appState;

  public ProgressBar(final AppState appState) {
    this.appState = appState;

    this.component = new JProgressBar(0, this.appState.numberOfQuestion());

    this.component.setStringPainted(true);
    this.component.setValue(
      this.appState.getQuizProgress().numberOfQuestionAnswered);
    this.component.setString(
      String.format(
        "Number of question answered: %d", 0));
  }

  public void update(final int newProgress) {
    if (newProgress > this.appState.getQuizProgress().numberOfQuestionAnswered) {
      this.component.setValue(newProgress);
      this.component.setString(
        String.format(
          "Number of question answered: %d", newProgress));
      this.component.updateUI();
    }
  }
}
