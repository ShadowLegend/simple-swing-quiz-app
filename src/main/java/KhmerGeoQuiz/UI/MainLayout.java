package main.java.KhmerGeoQuiz.UI;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Container;
import java.awt.Component;

import main.java.KhmerGeoQuiz.AppState;
import main.java.KhmerGeoQuiz.ReturnResult;
import main.java.KhmerGeoQuiz.Model.QuizQuestion;

import main.java.KhmerGeoQuiz.UI.MainHeader;
import main.java.KhmerGeoQuiz.UI.MainFooter;
import main.java.KhmerGeoQuiz.UI.QuestionImage;

final public class MainLayout {
  AppState appState;

  GridBagLayout layout;
  GridBagConstraints gridBagConstraints;
  Container container;

  public MainLayout(
    final AppState appState,
    final Container container,
    final GridBagConstraints gridBagConstraints
  ) {
    this.appState = appState;
    this.layout = new GridBagLayout();
    this.container = container;
    this.gridBagConstraints = gridBagConstraints;

    this.setup();
  }

  public GridBagLayout internalLayout() { return this.layout; }

  void setup() {
    this.setupHeader();

    this.setupBody();

    this.setupFooter();
  }

  void setupHeader() {
    MainHeader mainHeader = new MainHeader(this.appState);

    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.weighty = 0.2;
    this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;

    this.layout.setConstraints(
      mainHeader.internalComponent(), this.gridBagConstraints);

    this.container.add(mainHeader.internalComponent());
  }

  void setupBody() {
    QuestionImage questionImage =
      new QuestionImage(
        this.appState.getQuizQuestion(
          this.appState.getCurrentQuizId()).quizQuestionImageUrl());

    this.appState.listenTo(appStateChanges -> {
      if (appStateChanges.getPropertyName().equals(
          "QUIZ_PROGRESS:NAVIGATE_QUESTION")) {
        questionImage.update(
          ((QuizQuestion) appStateChanges.getNewValue()).quizQuestionImageUrl());
      }
    });

    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.weighty = 0.5;
    this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;

    this.layout.setConstraints(
      questionImage.internalComponent(), this.gridBagConstraints);

    this.container.add(questionImage.internalComponent());
  }

  void setupFooter() {
    MainFooter mainFooter = new MainFooter(this.appState);

    this.gridBagConstraints.weightx = 1.0;
    this.gridBagConstraints.weighty = 0.3;
    this.gridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;

    this.layout.setConstraints(
      mainFooter.internalComponent(), this.gridBagConstraints);
    
    this.container.add(mainFooter.internalComponent());
  }
}
