
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JLabel;
import javax.swing.JPanel;

import main.java.KhmerGeoQuiz.AppState;
import main.java.KhmerGeoQuiz.UI.UIComponent;
import main.java.KhmerGeoQuiz.Model.QuizQuestion;
import main.java.KhmerGeoQuiz.Model.UserQuizAnswer;

public class QuizDoneDialog extends UIComponent<JPanel> {
  AppState appState;

  public QuizDoneDialog(final AppState appState) {
    this.appState = appState;
    this.component = new JPanel();

    this.setup();
  }

  void setup() {
    int correctAnswer = 0;
    QuizQuestion[] quizQuestions = this.appState.getQuizQuestions();
    UserQuizAnswer userQuizAnswer;

    for (QuizQuestion quizQuestion: quizQuestions) {
      userQuizAnswer =
        this.appState.getUserAnswers().get(quizQuestion.quizQuestionId());

      if (quizQuestion.isCorrectAnswer(userQuizAnswer)) {
        ++correctAnswer;
      }
    }

    JLabel answerStatusText =
      new JLabel(
        String.format(
          "Corret answer: %d/%d",
          correctAnswer,
          this.appState.numberOfQuestion()
        ));

    this.component.add(answerStatusText);
  }
}
