
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JLabel;
import java.awt.Font;

import main.java.KhmerGeoQuiz.UI.UIComponent;

public class QuestionStatement extends UIComponent<JLabel> {
  String statement;

  public QuestionStatement(final String initialStatment) {
    this.statement = initialStatment;
    this.component = new JLabel(this.statement);
    this.component.setFont(new Font("Serif", Font.PLAIN, 20));
    this.component.setVisible(true);
  }

  public void update(final String statement) {
    if (!this.statement.equals(statement)) {
      this.statement = statement;
      this.component.setText(this.statement);
      this.component.revalidate();
      this.component.repaint();
    }
  }
}
