
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Event;

import main.java.KhmerGeoQuiz.UI.UIComponent;

public class Button extends UIComponent<JButton> {
  String buttonTitle;

  public Button(final String buttonTitle) {
    this.buttonTitle = buttonTitle;
    this.component = new JButton(this.buttonTitle);
  }

  public void onClick(final ActionListener actionListener) {
    this.component.addActionListener(actionListener);
  }
}
