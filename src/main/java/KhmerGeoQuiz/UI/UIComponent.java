
package main.java.KhmerGeoQuiz.UI;

import java.awt.Component;

public abstract class UIComponent<T extends Component> {
  protected T component;

  public T internalComponent() {
    return this.component;
  }
}
