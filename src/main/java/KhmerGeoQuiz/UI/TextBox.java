
package main.java.KhmerGeoQuiz.UI;

import javax.swing.JLabel;

import main.java.KhmerGeoQuiz.UI.UIComponent;

public class TextBox extends UIComponent<JLabel> {
  String text;

  public TextBox() {
    this.text = "";
    this.component = new JLabel();
  }

  public TextBox(final String text) {
    this.text = text;
    this.component = new JLabel(this.text);
  }

  public void update(final String text) {
    if (!this.text.equals(text)) {
      this.text = text;
      this.component.setText(this.text);
      this.component.revalidate();
      this.component.repaint();
    }
  }
}
