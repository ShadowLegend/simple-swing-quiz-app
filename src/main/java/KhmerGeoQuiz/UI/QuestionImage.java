
package main.java.KhmerGeoQuiz.UI;

import java.lang.ClassLoader;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Image;

import main.java.KhmerGeoQuiz.UI.UIComponent;

public class QuestionImage extends UIComponent<JLabel> {
  String imageUrl;
  ImageIcon image;

  final int defaultImageWidth;
  final int defaultImageHeight;

  public QuestionImage(final String imageUrl) {
    this.defaultImageWidth = 600;
    this.defaultImageHeight = 480;

    try {
      this.imageUrl = imageUrl;

      this.image = new ImageIcon();
      
      this.image.setImage(this.makeImageComponent(imageUrl));

      this.component = new JLabel();

      this.component.setIcon(this.image);

      this.component.setVisible(true);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  Image makeImageComponent(final String imageUrl) {
    try {
      ImageIcon imageIcon = new ImageIcon(
        ClassLoader.getSystemClassLoader().getResource(
          this.imageUrl).toURI().toURL());
      
      Image scaledImage =
        imageIcon.getImage().getScaledInstance(
          this.defaultImageWidth,
          this.defaultImageHeight,
          Image.SCALE_DEFAULT
        );

      return scaledImage;
    } catch (Exception exception) {
      exception.printStackTrace();

      return null;
    }
  }

  public void update(final String imageUrl) {
    if (!this.imageUrl.equals(imageUrl)) {
      this.imageUrl = imageUrl;
      this.image.setImage(this.makeImageComponent(imageUrl));
      this.component.setIcon(this.image);
      this.component.revalidate();
      this.component.repaint(); 
    }
  }
}
